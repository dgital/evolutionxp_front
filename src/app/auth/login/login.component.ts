import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import firebase from 'firebase/compat/app';
import { AuthService } from 'src/app/common/auth/auth.service';
import { WindowService } from 'src/app/common/window.service';
import { Observable } from 'rxjs';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { SecurityService } from 'src/app/common/auth/security.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None

})
export class LoginComponent implements OnInit {
  recaptchaVerifier: any;
  windowRef: any;
  verifyLoginCode: any;
  user: any;
  code: any;

  profileForm = new FormGroup({
    Phone: new FormControl('', Validators.required),

  });
  verifySMS = new FormGroup({
    Code: new FormControl('', Validators.required),

  });
  constructor(
    public auth: AngularFireAuth,
    private authService: AuthService,
    private windowService: WindowService,
    private toastr: ToastrService,
    private securityService: SecurityService,
    private router: Router


  ) {
    var Authorized = sessionStorage.getItem('IsAuthorized');

    if (Authorized == "true") {this.router.navigate(['./account/orders/list'])}
    console.log(Authorized);
  }

  ngOnInit(): void {
    this.windowRef = this.windowService.windowRef;
    this.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');

  }

  login() {
    this.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
  }
  logout() {
    this.auth.signOut();
  }
  onSubmitt() {
    //this.signIn(+573213989368);
    this.signIn(this.profileForm.value);
    //this.signIn(+573183990072);

  }
  onSubmittVerifySMS() {
    var data = this.verifySMS.value;

    //this.signIn(+573213989368);
    this.verifySMSCode(data);

  }
  signIn(phoneNumber: any) {
    phoneNumber = phoneNumber.Phone;
    phoneNumber.toString();
    const appVerifier = this.recaptchaVerifier;
    const phoneNumberString = "+57" + phoneNumber;
    console.log(phoneNumberString);
    firebase.auth().signInWithPhoneNumber(phoneNumberString, appVerifier)
      .then(async (confirmationResult) => {
        console.log(this.windowRef.confirmationResult = confirmationResult);
        console.log('Send Code');
        this.toastr.success('SMS', "Mensaje enviado a tu télefono");
        this.outLogin();
        this.inVerifySMS();

      })
      .catch(async (error) => {
        console.error("SMS not sent", error);
        this.toastr.error("SMS not sent", error);

      });

  }
  verifySMSCode(code: any) {
    console.log(code);
    this.windowRef.confirmationResult
      .confirm(code.Code)
      .then(async (result: any) => {
        this.user = result.user;
        console.log(this.user);
        this.securityService.SetAuthData(this.user);
        this.router.navigate(['./account/dashboard']);
      }).catch((error: any) => {
        console.log(error);
      });
  }
  signOut() {
    firebase.auth().signOut().then(() => {
      // Sign-out successful.
      console.log("Sign-out successful");
      sessionStorage.clear();
    }).catch((error) => {
      // An error happened.
    });
  }
  outLogin() {
    var login = document.getElementById("loginContainer");
    if (login) {
      login.classList.add("hidde");
      login.classList.remove("show");
    }

  }
  inVerifySMS() {
    var sms = document.getElementById("verifySMS");
    if (sms) {
      sms.classList.add("show");
      sms.classList.remove("hidde");

    }
  }
}

import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
  encapsulation:ViewEncapsulation.Emulated
})
export class AuthComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}

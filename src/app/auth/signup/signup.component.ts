import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import firebase from 'firebase/compat/app';
import { AuthService } from 'src/app/common/auth/auth.service';
import { WindowService } from 'src/app/common/window.service';
import { Observable } from 'rxjs';
import { FormControl, FormGroup, Validators } from '@angular/forms';
AngularFireAuth
firebase
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  recaptchaVerifier: any;
  windowRef: any;
  verifyLoginCode: any;
  user: any;
  code: any;

  profileForm = new FormGroup({
    Phone: new FormControl('', Validators.required),

  });
  verifySMS = new FormGroup({
    Code: new FormControl('', Validators.required),

  });
  constructor(public auth: AngularFireAuth,
    private authService: AuthService,
    private windowService: WindowService,
  ) { }
  disableOTPSendButton = true;
  ngOnInit(): void {
    this.windowRef = this.windowService.windowRef;
    this.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container');
  }

  login() {
    this.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
  }
  logout() {
    this.auth.signOut();
  }
  onSubmitt() {
    //this.signIn(+573213989368);
    this.signIn(this.profileForm.value);
    //this.signIn(+573183990072);

  }
  onSubmittVerifySMS() {
    var data = this.verifySMS.value;

    //this.signIn(+573213989368);
    this.verifySMSCode(data);

  }
  signIn(phoneNumber: any) {
    phoneNumber = phoneNumber.Phone;
    phoneNumber.toString();
    const appVerifier = this.recaptchaVerifier;
    const phoneNumberString = "+57" + phoneNumber;
    console.log(phoneNumberString);
    firebase.auth().signInWithPhoneNumber(phoneNumberString, appVerifier)
      .then(async (confirmationResult) => {
        console.log(this.windowRef.confirmationResult = confirmationResult);
        console.log('Send Code');

      })
      .catch(function (error) {
        console.error("SMS not sent", error);
      });

  }
  verifySMSCode(code: any) {
    console.log(code);
    this.windowRef.confirmationResult
      .confirm(code.Code)
      .then(async (result: any) => {
        this.user = result.user;
        console.log(this.user);

      }).catch((error: any) => {
        console.log(error);
      });
  }
  signOut() {
    firebase.auth().signOut().then(() => {
      // Sign-out successful.
      console.log("Sign-out successful");
    }).catch((error) => {
      // An error happened.
    });
  }
}

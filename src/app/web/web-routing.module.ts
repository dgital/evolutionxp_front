import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from '../auth/login/login.component';
import { AuthGuardService } from '../common/auth/auth-guard-service.service';
import { HomeComponent } from './home/home.component';
import { WebComponent } from './web.component';

const routes: Routes = [{

  path:'',
  component:WebComponent,
  //redirectTo:'/account',

  children:[{
    path: '',
    component:HomeComponent,

  }],
  
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WebRoutingModule { }

import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Router } from '@angular/router';
import firebase from 'firebase/compat/app';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(
    public auth: AngularFireAuth,
    private router: Router,
    private toastr: ToastrService

  ) { }

  ngOnInit(): void {
  }
  signOut() {
    firebase.auth().signOut().then(() => {
      // Sign-out successful.
      console.log("Sign-out successful");
      sessionStorage.clear();
      this.router.navigate(['./auth/login']);
      this.toastr.success('Se ha cerrado correctamente','LogOut');
    }).catch((error: any) => {
      // An error happened.
    });
  }
}

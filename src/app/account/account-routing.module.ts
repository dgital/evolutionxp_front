import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DasboardComponent } from '../account/dasboard/dasboard.component';
import { AuthGuardService } from '../common/auth/auth-guard-service.service';
import { AccountComponent } from './account.component';
import { FollowComponent } from './orders/follow/follow.component';
import { ListComponent } from './orders/list/list.component';
import { OrdersComponent } from './orders/orders.component';

const routes: Routes = [{
  path: '',
  component: AccountComponent,
  
  children: [{
    path: 'orders',
    component: OrdersComponent,
    children: [{
      path: 'list',
      component: ListComponent
    }, {
      path: 'follow/:id',
      component: FollowComponent
    }]
  },
{
  path:'dashboard',
  component:DasboardComponent
}]
},
{
  loadChildren: () => import('./orders/orders.module').then(m => m.OrdersModule)
},];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountRoutingModule { }

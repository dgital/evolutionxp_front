import { Component, OnInit } from '@angular/core';
import {PrimeIcons} from 'primeng/api';
import { OrdersService } from 'src/app/common/orders/orders.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {
  products:any;
  constructor(private items: OrdersService) { }

  ngOnInit(): void {
    this.products = this.items.getAll();
  }

}

import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FollowComponent } from './follow/follow.component';
import { ListComponent } from './list/list.component';
import { OrdersComponent } from './orders.component';

const routes: Routes = [
  {
    path: '',
    component: OrdersComponent,
    children: [{
      path: 'order',
      component: FollowComponent
    }, {
      path: 'list',
      component: ListComponent
    }]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrdersRoutingModule { }

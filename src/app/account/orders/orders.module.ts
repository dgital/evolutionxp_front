import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OrdersRoutingModule } from './orders-routing.module';
import { ListComponent } from './list/list.component';
import { OrdersComponent } from './orders.component';

import { FollowComponent } from './follow/follow.component';



@NgModule({
  declarations: [
    ListComponent,
    OrdersComponent,
    
    FollowComponent,
    
  ],
  imports: [
    CommonModule,
    OrdersRoutingModule
  ]
})
export class OrdersModule { }

import { Component, OnInit } from '@angular/core';
import { OrdersService } from 'src/app/common/orders/orders.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  products:any;
  constructor(  private items: OrdersService  
  ) { 
    this.products = this.items.getAll();
  }

  ngOnInit(): void {
    this.products = this.items.getAll();

  }

}

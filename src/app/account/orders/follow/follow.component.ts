import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OrdersService } from 'src/app/common/orders/orders.service';

@Component({
  selector: 'app-follow',
  templateUrl: './follow.component.html',
  styleUrls: ['./follow.component.scss']
})
export class FollowComponent implements OnInit {
  id = +this.route.snapshot.params.id;
  follow: any;
  product:any;
  constructor(
    private items: OrdersService,
    private router: Router,
    private route: ActivatedRoute,

  ) { }

  ngOnInit(): void {
    this.get();
  }
  get() {
    this.id;
    var e = this.items.get(this.id);
    this.product = e;
    this.listFollow(e);
  }
  listFollow(items: any) {
    var i=0;
    //items.forEach(async(element:any) => {
      this.follow =items.follow;
    /*  if(i>0){
        i=0
      }
      i++;
    });*/
  }
}

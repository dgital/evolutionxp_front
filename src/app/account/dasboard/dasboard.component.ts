import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OrdersService } from 'src/app/common/orders/orders.service';

@Component({
  selector: 'app-dasboard',
  templateUrl: './dasboard.component.html',
  styleUrls: ['./dasboard.component.scss']
})
export class DasboardComponent implements OnInit {
  products: any;
  id = +this.route.snapshot.params.id;

  constructor(
    private items: OrdersService,
    private router: Router,
    private route: ActivatedRoute,

  ) {
    this.products = this.items.getAll();
  }

  ngOnInit(): void {
    this.products = this.items.getAll();
  }

}

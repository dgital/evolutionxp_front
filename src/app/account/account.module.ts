import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountRoutingModule } from './account-routing.module';
import { OrdersComponent } from './orders/orders.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { AccountComponent } from './account.component';
import {NgbPaginationModule, NgbAlertModule} from '@ng-bootstrap/ng-bootstrap';
import {TimelineModule} from 'primeng/timeline';
import { FollowComponent } from './orders/follow/follow.component';
import { DasboardComponent } from './dasboard/dasboard.component';

@NgModule({
  declarations: [
    AccountComponent,
    FooterComponent,
    HeaderComponent,
    DasboardComponent
  ],
  imports: [
    CommonModule,
    AccountRoutingModule,
    NgbPaginationModule,
    NgbAlertModule,
    TimelineModule
  ]
})
export class AccountModule { }

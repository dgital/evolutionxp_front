import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from './common/auth/auth-guard-service.service';

const routes: Routes = [{
  path: '',
  loadChildren: () => import('./web/web.module').then(m => m.WebModule),

},
{
  path: 'auth',
  loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)
},
{
  path: 'admin',
  loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule)
},
{
  path: 'account',
  loadChildren: () => import('./account/account.module').then(m => m.AccountModule),
  canActivate:[AuthGuardService]
}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

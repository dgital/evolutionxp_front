import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { StorangeService } from './storange.service';
@Injectable({
  providedIn: 'root'
})
export class SecurityService {
  IsAuthorized: any;
  private authSource = new Subject<boolean>();
  authChanllenge$ = this.authSource.asObservable();
  constructor(private storeService2: StorangeService, private router: Router) {
    if ( this.storeService2.retrieve('IsAuthorized') !== '') {
      this.IsAuthorized = this.storeService2.retrieve('IsAuthorized');
      this.authSource.next(this.IsAuthorized);
    }
  }
  GetToken() {
    return this.storeService2.retrieve('authData');
  }
  public ResetAuthData() {
    this.storeService2.store('authData', '');
    this.IsAuthorized = false;
    this.storeService2.store('IsAuthorized', false);
  }
  public SetAuthData(token: any) {
    this.storeService2.store('authData', token);
    this.IsAuthorized = true;
    this.storeService2.store('IsAuthorized', true);

    this.authSource.next(true);
  }
  public LogOff() {
    this.ResetAuthData();
    this.authSource.next(false);
    this.router.navigate(['/login']);
  }
}

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {
  p: any;
  items = [
    {
      id: 1,
      name: "Ensalada de Frutas Super",
      storeName: 'Fruteria Mickey del Norte - Calle 129Fruteria Mickey del Norte - Calle 129',
      price: 35548,
      date: "2021-09-07 15:30:00.0 ",
      hour: "15:30",
      img: '2091656839-1592343384386.webp',
      status: 'Entregada',
      follow: [
        {
          name: "Solicitud Recibida",
          date: "2021-09-07 15:30:00.0 ",
          hour: "15:30",
          description: 'Se ha recibido el pedido, ahora empezara a gestionar '
        },
        {
          name: "Praparando pedido",
          date: "2021-09-07 15:43:00.0 ",
          hour: "15:40",
          description: 'Los profesionales estan preparando el pedido a detalle. '

        },
        {
          name: "Repartidor asignado",
          date: "2021-09-07 15:57:00.0 ",
          hour: "15:56",
          description: 'Se ha asignado a un repartidor'

        },
        {
          name: "En Camino",
          date: "2021-09-07 16:05:00.0 ",
          hour: "15:56",
          description: 'El repartido ha salido con ti pedido'

        },
        {
          name: "Tu pedido ha llegado ",
          date: "2021-09-07 16:30:00.0 ",
          hour: "16:02",
          description: 'Tu pedido se encuentra en tu domicilio'

        },
        {
          name: "Entregado ",
          date: "2021-09-07 16:34:00.0 ",
          hour: "16:04",
          description: 'Tu pedido se te ha entregado con éxito, que lo disfrutes'

        }
      ]


    },
    {
      id: 2,
      name: "Fruta y frutas",
      storeName: 'Lolita Fruta',
      price: 35548,
      date: "Viernes, 16 de julio de 2021",
      hour: "15:30",
      img: '2091656839-1592343384386.webp',
      status: 'Entregada',
      follow: [
        {
          name: "Solicitud Recibida",
          date: "2021-09-07 15:30:00.0 ",
          hour: "15:30",
          description: 'Se ha recibido el pedido, ahora empezara a gestionar '
        },
        {
          name: "Praparando pedido",
          date: "2021-09-07 15:43:00.0 ",
          hour: "15:40",
          description: 'Los profesionales estan preparando el pedido a detalle. '

        },
        {
          name: "Repartidor asignado",
          date: "2021-09-07 15:57:00.0 ",
          hour: "15:56",
          description: 'Se ha asignado a un repartidor'

        },
        {
          name: "En Camino",
          date: "2021-09-07 16:05:00.0 ",
          hour: "15:56",
          description: 'El repartido ha salido con ti pedido'

        },
        {
          name: "Tu pedido ha llegado ",
          date: "2021-09-07 16:30:00.0 ",
          hour: "16:02",
          description: 'Tu pedido se encuentra en tu domicilio'

        },
        {
          name: "Entregado ",
          date: "2021-09-07 16:34:00.0 ",
          hour: "16:04",
          description: 'Tu pedido se te ha entregado con éxito, que lo disfrutes'

        }
      ]
    }
  ];
  constructor() {

  }
  getAll() {
    return this.items;
  }
  get(id: any) {
    console.log(id);
    this.items.forEach(el => {
      if (el.id == id) {
        this.p = el;
      }

    });
    return this.p
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { DasboardComponent } from './dasboard/dasboard.component';
import { AdminComponent } from './admin.component';
import { OrdersComponent } from './orders/orders.component';


@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    DasboardComponent,
    AdminComponent,
    OrdersComponent
  ],
  imports: [
    CommonModule,
    AdminRoutingModule
  ]
})
export class AdminModule { }

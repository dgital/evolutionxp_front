// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    /* apiKey: "AIzaSyAw9o5kB7t5AcF3hWiKEM8bU9N2xP67gbs",
     authDomain: "revista-29fbc.firebaseapp.com",
     projectId: "revista-29fbc",
     storageBucket: "revista-29fbc.appspot.com",
     messagingSenderId: "267586472675",
     appId: "1:267586472675:web:e7de51360dd1b2e18713d6",
     measurementId: "G-4XMHGFBHGR"*/
    apiKey: "AIzaSyAJGu4J8UaPPq8AO-89NlD8HlQJuW_H0p4",
    authDomain: "papi-faac5.firebaseapp.com",
    projectId: "papi-faac5",
    storageBucket: "papi-faac5.appspot.com",
    messagingSenderId: "16133562453",
    appId: "1:16133562453:web:47e84f2846f8c3bf6db4dc",
    measurementId: "G-EQTE234489"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.


// Firebase services + enviorment module


